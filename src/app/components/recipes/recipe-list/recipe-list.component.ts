import { Component, OnInit } from '@angular/core';
import { Recipe } from '../recipe.model';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {

  recipes: Recipe[] = [
    new Recipe("Victoria sandwich", "Lorem ipsum dolor sit amet consectetur adipisicing elit.", "https://keyassets-p2.timeincuk.net/wp/prod/wp-content/uploads/sites/63/2018/09/Victoria-sandwich-1.jpg")
  ];


  constructor() { }

  ngOnInit() {
  }

}
